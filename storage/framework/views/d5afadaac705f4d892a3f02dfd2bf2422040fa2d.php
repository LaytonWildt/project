<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <h2 class="text-lg font-semibold">Standard Contact</h2>
    </head>
    <body>
    <?php
if (! isset($_instance)) {
    $html = \Livewire\Livewire::mount('contact-form', [])->html();
} elseif ($_instance->childHasBeenRendered('C43bvYq')) {
    $componentId = $_instance->getRenderedChildComponentId('C43bvYq');
    $componentTag = $_instance->getRenderedChildComponentTagName('C43bvYq');
    $html = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('C43bvYq');
} else {
    $response = \Livewire\Livewire::mount('contact-form', []);
    $html = $response->html();
    $_instance->logRenderedChild('C43bvYq', $response->id(), \Livewire\Livewire::getRootElementTagName($html));
}
echo $html;
?>
    <?php echo \Livewire\Livewire::scripts(); ?>

    </body>
</html>
<?php /**PATH C:\xampp\htdocs\MywebApp\resources\views/welcome.blade.php ENDPATH**/ ?>